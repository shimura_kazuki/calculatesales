package jp.alhinc.shimura_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

//	    コマンドライン引数が渡せているか
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchName = new HashMap<String, String>();
		Map<String, Long>  branchSales = new HashMap<String, Long>();

//      ファイル読み込み処理
		BufferedReader br = null;
		if (!readFile(args[0], branchName, branchSales)) {
			return;
		}


//      売上ファイル読み込み処理

		File sale    = new File(args[0]);
		File[] files = sale.listFiles();

		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length ; i++) {
			String fileName = files[i].getName();
			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
			}
		}

		Collections.sort(rcdFiles);

//	    例外処理（売上ファイルが連番になっているか）(2.1)
		for (int i = 0; i < (rcdFiles.size() - 1); i++) {
			int before = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int later = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if ((later - before) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));
				String line;
				ArrayList<String> fileContents = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					fileContents.add(line);
				}

//              行数のチェック(2.4)
				if (fileContents.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
//	            支店コードの存在確認(2.3)
				if (!branchName.containsKey(fileContents.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				Long saleSum;
				saleSum = branchSales.get(fileContents.get(0)) + Long.parseLong(fileContents.get(1));
				if (!fileContents.get(1).matches("^[0-9]$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
//	            金額が10桁になった時の例外処理（2.2）
				if ( saleSum >= 1000000000L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put( fileContents.get(0), saleSum );
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
//      ファイル書き込み処理
		if (!writeFile(args[0], branchName, branchSales)) {
			return;
		}
	}


//	ファイル書き込み処理のメソッドを分割

	public static boolean writeFile( String path, Map<String,String> branchName, Map<String, Long> branchSales ) {
		BufferedWriter bw = null;

		try {
			File file = new File(path, "branch.out");
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key: branchSales.keySet()) {
				bw.write(key + " , " + branchName.get(key) + " , " + branchSales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}

//	支店定義ファイル読み込み処理のメソッドを分割
	public static boolean readFile( String path, Map<String,String> branchName, Map<String, Long> branchSales ) {

		BufferedReader br = null;
		try {
			File file = new File(path, "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
					String[] items = (line).split(",");

				if ((items.length != 2) || (!items[0].matches("^[0-9]{3}"))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				branchName.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


}
